# conf_logipopkeys

This is my own [Solaar](https://github.com/pwr-Solaar/Solaar) rules for keyboard [logitech pop keys](https://www.logitech.com/en-us/products/keyboards/pop-keys-wireless-mechanical.920-010708.html) on linux using gnome as windows manager.

## Getting started

1. Install [Emote](https://github.com/tom-james-watson/Emote) package
2. Install [Solaar](https://github.com/pwr-Solaar/Solaar)
3. Copy [config.yaml](./config.yaml) and [rules.yaml](./rules.yaml) into `~/.config/solaar`
4. Verify in solaar if rules are used.
